const express = require('express');
const app = express();
app.use(express.json());
const axios = require('axios');

const ingressoPorClienteId = {};
const {
    v4: uuidv4
} = require('uuid');

const funcoes = {
    Ingressolassificada: (ingresso) => {
        const ingressos =
            ingressosPorClienteId[ingresso.clienteId];
        const obsParaAtualizar = ingresso.find(o => o.id ===
            ingresso.id)
        obsParaAtualizar.status = ingresso.status;
        axios.post('http://localhost:10000/eventos', {
            tipo: "IngressoAtualizado",
            dados: {
                id: ingresso.id,
                texto: ingresso.texto,
                clienteId: ingresso.clienteId,
                status: ingresso.status
            }
        });
    }
}

app.post("/eventos", (req, res) => {
    try {
        funcoes[req.body.tipo](req.body.dados);
    } catch (e) {}
    res.status(200).send({
        msg: "ok"
    });
});


app.put('/lembretes/:id/ingresso', async (req, res) => {
    const idObs = uuidv4();
    const {
        texto
    } = req.body;

    const ingressoPorCliente =
        ingressoPorClienteId[req.params.id] || [];
    ingressoPorCliente.push({
        id: idObs,
        texto,
        status: 'aguardando'
    });


    ingressoPorClienteId[req.params.id] =
        ingressoDoCliente;
    await axios.post('http://localhost:10000/eventos', {
        tipo: "IngressoCriado",
        dados: {
            id: idObs,
            texto,
            ingressoId: req.params.id,
            status: 'aguardando'
        }
    })
    res.status(201).send(ingressoPorClienteId);
});
app.get('/cliente/:id/ingresso', (req, res) => {
    res.send(ingressoPorClienteId[req.params.id] || []);

});
app.listen(5000, (() => {
    console.log('Ingresso. Porta 5000');
}));